function Y = UHT2(X, lvl)
% UHT2  - Undecimated 2D Haar Transform
%
%   Usage:
%       W  = UHT2(im, 5);   % direct transform
%       im = UHT2(W);   	% inverse transform

    if (exist('lvl', 'var') && ~isempty(lvl))
        Y = forward(X, lvl);
    else
        Y = backward(X);
    end;
end

% Forward transform
function W = forward(X, lvl)

    % Initializations
    if (size(X, 3) > 1)
        error('Only implemented for gray images');
    end;
    [M, N] = size(X);
    W = zeros(M, N, lvl, 2, 2);

    % Apply Haar
    sumdiff = @(x, y) deal(x + y, x - y);
    haar1 = @(x, n) sumdiff(x, circshift(x, 2 ^ (n-1)));

    % Processing
    for n = 1:lvl
        [L, H] = haar1(X' / 2, n);
        [W(:,:,n,1,1), W(:,:,n,2,1)] = haar1(L', n);
        [W(:,:,n,1,2), W(:,:,n,2,2)] = haar1(H', n);
        X = W(:,:,n,1,1);
    end;
end

% Backward transform
function X = backward(W)

    % Initializations
    [M, N, lvl, four] = size(W);
    X = W(:,:,end,1,1);
    ihaar1L = @(x, n) x + circshift(x, -2 ^ (n-1));
    ihaar1H = @(x, n) x - circshift(x, -2 ^ (n-1));

    % Processing
    for n = lvl:-1:1
        L = (ihaar1L(X, n) + ihaar1H(W(:,:,n,2,1), n))';
        H = (ihaar1L(W(:,:,n,1,2), n) + ihaar1H(W(:,:,n,2,2), n))';
        X = (ihaar1L(L, n) + ihaar1H(H, n))' / 8;
    end;

end