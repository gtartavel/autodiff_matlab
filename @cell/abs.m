function Y = abs(X)
% ABS   - absolute value or complex modulus of a cells, element-wise.
% 
%   Usage:
%       Y = abs(X);
    Y = cellop({@abs X});
end
