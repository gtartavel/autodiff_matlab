function [x, flag, R] = cg(A, b, maxit, tol, x0, M1, M2, sym)
% CG    - conjugate gradient algorithm
%
%   Solve Ax = b for a symmetric positive-definite matrix A of size NxN
%   	or apply the pseudo-inverse for a non-square matrix of size MxN
%
%   Usage:
%       x = cg(A, b, maxit);
%       x = cg(A, b, maxit, tol, x0);
%       x = cg(A, b, maxit, tol, x0, [], [], true);
%       [x, flag] = cg(...);
%       [x, flag, R] = cg(...);
%
%   Arguments:
%       A       - a linear operator
%       b       - right-hand side element(s)
%       maxit   - maximum number of iteration
%                 theoretical, N iterations are enough
%       tol     - tolerance on the residual || b - Ax ||
%                 default is 1e-6
%       x0      - initial guess for x0
%                 default is zeros
%       sym     - if true, handle non-sym. or non-pos. matrix through (A'A)x = A'b
%
%   Returns:
%       x       - approximation of the solution of Ax = b
%       flag    - information about the convergence
%       R       - vector of residual across the iteration
%
%   Arguments format:
%       A   ---   the linear operator can be:
%        	. a square symmetric positive-definite matrix (faster convergence)
%           . a matrix, non-sym. or non-pos. if 'sym' flag is turned on
%           . a handle of a square positive-definite operator (faster convergence)
%           . a cell {L, La} made of a linear operator and its adjoint
%       b   ---   the right-hand side element(s) can be:
%           . a column vector when A is a matrix
%           . a matrix (severa column vectors) when A is a matrix
%           . any argument for A when A is a handle
%           . any argument for L when A = {L, La} is a cell of handles
%       flag --   information about the convergence
%           . 0 if tolerance 'tol' is reached
%           . 1 if stopped after 'maxit' iterations
%           . 3 if the residual stagnated
%
%   Non-squared case:
%       When A is a MxN matrix, the following system is solved.
%       If M > N (over-constraint, type > 0):
%           A'Ax = A'b is solved
%       If M < N (under-constraint, type < 0):
%           Ax = b is solved for x = A'z
%           AA'z = b is the resulting system


    % Settings
    kr = 16;            % residual recomputed each kr steps
    kd = 0;             % ignoring conjugation each kd steps
    k_ = 64;            % stop if no increase after k_ steps
    debugFcn = @disp;% @(x) []; % use '@disp' to see debug informations;

    % Functions
    mult = @(t,x) t * x;
    dotp = @(xa,xb) xa(:)' * xb(:);
    novar = @(s) ~exist(s, 'var');
    isfun = @(h) isa(h, 'function_handle');

    % Tolerance
    if (novar('tol') || isempty(tol))
        tol = 1e-6;
    end;


    %%%%%  ARGUMENT FORMATING  %%%%%
    type = 0;      	% number of over-determination
    flag = 1;       % flag for the convergence
    nosym = true;   % do not symmetrize

    % Pre-conditionners
    % FIXME: not implemented yet
    iM = @(x) x;
    if not(novar('M1') || isempty(M1)) || not(novar('M2') || isempty(M2))
        warning('CG:NotYet', 'Preconditioning not implemented yet, ignored.');
    end;

    % Turn A into a function
    if (isnumeric(A))
        % Matrix case
        Amat = A;
        dotp = @(x,y) sum(x .* y, 1);
        mult = @(t,x) bsxfun(@times, t, x);
        type = sign(size(A,1) - size(A,2));
        nosym = novar('sym') || ~sym;
        if (type == 0 && nosym)
            debugFcn('Squared matrix');
            A = @(x) Amat * x;
        elseif (type >= 0)
            debugFcn('Over-constrained matrix');
            At = Amat';
            [A, Ai] = deal(@(x) Amat*x, @(y) At*y);
        else
            debugFcn('Under-constrained matrix');
            At = Amat';
            [Ai, A] = deal(@(x) Amat*x, @(y) At*y);
        end;
    elseif (iscell(A) && length(A) == 2)
        % Function & adjoint
        type = sign(numel(b) - numel(A{2}(b)));
        nosym = false;  % force symmetrize
        if (type == 0 && nosym)
            debugFcn('Operator');
            A = A{1};
        elseif (type >= 0)
            debugFcn('Over-constrained operator');
            [A, Ai] = deal(A{:});
        else
            debugFcn('Under-constrained operator');
            [Ai, A] = deal(A{:});
        end;
    elseif (~isfun(A))
        % Error case
        error('Invalid type for argument ''A''.');
    end;

    % Specific settings for each type
    if (type == 0 && nosym)
        Ai = @(x) x;
        dotpaux = dotp;
    elseif (type >= 0)
        b = Ai(b);
        dotpaux = @(x,Ax) dotp(Ax,Ax);
    else
        dotpaux = @(x,Ax) dotp(Ax,Ax);
    end;

    % Initial point
    if (novar('x0') || isempty(x0))
        x0 = zeros(size(b));
    elseif (type < 0)
        x0 = Ai(x0);
    end;


    %%%%%  CONJUGATE GRADIENT ALGORITHM  %%%%%      % Pseudo-code

    % Initialization
    x = x0;                                         % x = ...
    r = b - Ai(A(x));       % residual              % r = b - Ax
    d = iM(r);              % direction             % d = r
    rr = dotp(r,r);

    % Iterate until convergence
    R = zeros(maxit, 1);
    km = 1;
    for k = 1:maxit
        if (max(rr) <= tol ^ 2)
            debugFcn('Converged');
            [R, flag] = deal(R(1:k-1), 0);
            break;
        end;

        % Update x
        Ad = A(d);
        t = rr ./ dotpaux(d, Ad);                   % t = r'r / d'Ad
        x = x + mult(t, d);                         % x = x + t.d

        % Update r
        if (mod(k, kr))
            r = r - mult(t, Ai(Ad));                % r = r - t.Ad
        else
            r = b - Ai(A(x));   % recompute it
        end;

        % Update d
        iMr = iM(r);
        [rro,rr] = deal(rr, dotp(r, iMr));          % R = previous r
        u = (rr ./ rro) * logical(mod(k, kd));      % u = r'r / RR
        d = iMr + mult(u, d);                       % d = r + u.d

        % Keep track of rr
        R(k) = sum(rr);
        if (R(k) <= R(km))
            km = k;
            xm = x;
        elseif (k > km + k_)
            debugFcn('Stagnated');
            [R, flag] = deal(R(1:km), 3);
            break;
        end;
    end;
    x = xm;

    % Multiply by At (for right pseudo-inverse)
    if (type < 0)
        x = A(x);
    end;

    % End
    debugFcn(sprintf('-> %d iteration, residual is %f', length(R), sqrt(R(end))));
end
