function [Y, DY, DYs] = steerpyramid(X, scales, filters)
% STEER PYRAMID     - Simoncelli Steerable Pyramid
%
%   Usage:
%       [pyr, decompo, recon] = steerpyramid(im, scales, filters);
%
%   Arguments:
%       im      - input image
%       scales  - number of scales
%       filters - filters function, see 'buildSpyr'
%
%   Return values:
%       pyr         - the pyramid, as a cell containing all subbands
%       decompo     - function: decomposition
%       recon       - adjoint: reconstruction
%
%   See also:
%       buildSpyr (from pyrTools)

    edges = 'circular';
    [P, PI] = buildSpyr(X, scales, filters, edges);
    Y = pyr2cell(P, PI, true);

    clear P;
    DY = @(dX) pyr2cell(buildSpyr(dX, scales, filters, edges), PI, true);
    DYs = @(dY) reconSpyr(full(cell2pyr(dY)), PI, filters, edges);
end

% Convert a cell of subbands to a pyramid
function pyr = cell2pyr(bands)
% CELL2PYR    -
%
%   Usage:
%       pyr = cell2pyr(bands)
%
%   See also:
%       pyr2cell

    cells = cellfun(@(x) x(:), bands, 'UniformOutput', false);
    pyr = cat(1, cells{:});

end

% Convert a pyramid and indices to a cell of subbands.
function bands = pyr2cell(pyr, indices, vectorize)

    Nlvl = spyrHt(indices);             % number of levels
    Nb = spyrNumBands(indices);         % number of bands
    bands = cell(Nlvl * Nb + 2, 1);     % output

    if (~exist('vectorize', 'var') || ~vectorize)
        redim = @(B) B;
    else
        redim = @(B) B(:);
    end;

    bands{1} = redim(spyrHigh(pyr, indices));
    bands{end} = redim(pyrLow(pyr, indices));
    for lvl = 1:Nlvl
        for b = 1:Nb
            bands{1+b + Nb*(lvl-1)} = redim(spyrBand(pyr, indices, lvl, b));
        end;
    end;

end