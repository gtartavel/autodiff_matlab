function C = max(A, B)
% MAX   - element-wise maximum between cells, or between a cell and a scalar.
%
%   Usage:
%       C = max(A, B);  % A or B may be a scalar
%
%   Warning:
%       Do NOT support maximum along a dimension.
   C = cellscalarop({@max A B});
end
