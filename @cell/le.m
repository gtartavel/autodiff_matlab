function C = le(A, B)
% LE (<=)   - element-wise '<=' between cells, or between a cell and a scalar.
%
%   Usage:
%       C = A <= B;  % A or B may be a scalar
%       C = (A <= B);
%       C = le(A, B);
    C = cellscalarop({@le A B});
end
