function C = mtimes(A, B)
% MTIMES (*)    - matrix multiplication between cells' sub-arrays, or between a cell and a scalar.
%
%   Usage:
%       C = A * B;  % A or B may be a scalar
%       C = mtimes(A, B);
    C = cellscalarop({@mtimes A B});
end
