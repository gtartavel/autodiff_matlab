Automatic differentiation in Matlab
===================================

This class implements differentiable functions and store their derivative.
It handles operations between such functions (e.g. composition).

The class provides a minimization routine to find the minimum of a function.

Author: Guillaume Tartavel
See also: my thesis, Chap. IV

# Documentation

The Matlab files are self documented.
The documentation contains examples.
For instance:

    >> help Function            # display general help on the 'Function' class
    >> help Function.Function   # help for the constructor of 'Function'
    >> help Function.minimize   # help for the 'minimize' method of 'Function'
    

# Content

    -  readme.md                -- you are reading it
    -  Function.m               -- main class
    -  CellFunction.m           -- extension of Function to cells
    -  SparseCodingFunction.m   -- return a sparse-coding 'Function'
    -  perform_bfgs.m           -- auxiliary            -- BFGS algorithm
    -  cg.m                     -- auxiliary/unused     -- conjugate gradient algorithm
    +- FunctionList/            -- a small library of standard functions
    |   +  <files>.m            -- functions which can be used in 'Function'
    +- @cell/                   -- extension of common operators to cells
    |   +  <files>.m            -- overloaded functions and operators for cells
