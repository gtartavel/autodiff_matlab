function C = ne(A, B)
% NE (~=)   - element-wise unequality between cells, or between a cell and a scalar.
%
%   Usage:
%       C = A ~= B;  % A or B may be a scalar
%       C = (A ~= B);
%       C = ne(A, B);
    C = cellscalarop({@ne A B});
end
