function C = eq(A, B)
% EQ (==)   - element-wise equality between cells, or between a cell and a scalar.
%
%   Usage:
%       C = A == B;  % A or B may be a scalar
%       C = (A == B);
%       C = eq(A, B);
    C = cellscalarop({@eq A B});
end
