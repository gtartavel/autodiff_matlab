classdef Function
% FUNCTION      - a class to deal with functions.
%
%   Constuctor:
%       F = Function(name, ...)     - creates a function from its name.
%       See <a href="matlab: help Function.Function">constructor details</a>
%
%   Methods:
%       Methods provide function handles.
%       See the <a href="matlab: methods(Function)">list of methods</a>.
%       See the <a href="matlab: properties(Function)">list of properties</a>.
%
%   Operators:
%       +   - sum of functions/constants:   H = F + G + 42;
%       -   - difference/negation:          H = -F - G - 42;
%       *   - multiplication (matricial):   H = F * G * 3.14;
%       o   - composition of functions:     H = F.o(G);
%       x   - affine scaling:               H = F.x(3.14);
%       :   - composition (same)            H = F : G;
%             beware of Matlab's priority:  E+F:G*H  means  (E+F) : (G*H)
%
%   Other tools:
%       Function.cellmapreduce  - recursive extension of 'cellfun'
%
%   See also:
%       Function.Function
%       Function.dectype
%       Function.get
%       Function.eval
%       Function.check
%       Function.minimize
%
%   (c) Guillaume Tartavel, 2012--2014

% TODO:
%   * extend types deduction/checking, like:
%       - operators, eg: +, -, x, *, etc.
%       - Functions, eg: '{}', {F, G}, polynoms, etc.

properties (Constant)
    CHECK_TYPE_ON_DECLARE = true;  % do not check whether declared types are correct
    CHECK_TYPE_ON_CALL = true;     % do not check whether arguments types are correct
    DISABLE_TYPES = false;         % totally disable types
end;

% Main getters
properties (Dependent)

    % Handles
    value;          % handle to: X -> value(X)
    gradient;       % handle to: X -> gradient(X)
    derivative;     % handle to: X -> dX -> derivative(X)(dX)
    deriv_star;     % handle to: X -> dY -> adjoint-of-derivative(X)(dY)

    % Types
    is_typed;       % true iff typed are provided
    type_in;        % type of the input (X, dX, gradient(X), ...)
    type_out;       % type of the output(function value, dY, ...)
end;

% Main methods
methods (Access=public)

    % Constructor
    function this = Function(arg, varargin)
    % FUNCTION  - constructor.
    %
    %   Usage:
    %       G = Function(F);         	% Copy of a Function F
    %       G = Function({F, ...});   	% Function @(X) {F(X), ...(X)}
    %       F = Function(fcn, ...);     % User-defined function, see below
    %
    %       % General functions:
    %       F = Function('{}', ...);        % Function @(X) X{...}
    %       F = Function('(:)');            % Function @(X) X(:)
    %       F = Function('Id');             % Identity function
    %       F = Function('Linear', A);      % Linear, from matrix A
    %       F = Function('Linear', L, Ls);  % Linear, from fcn L / adjoint Ls
    %       F = Function('Reshape', ...);   % Matlab reshape(X, ...)
    %       F = Function('Polynom', ...);   % Polynomial (works with cell)
    %
    %       % Constant / polyomial functions:
    %       F = Function(C);                % Constant function
    %       F = Function(C, 0);             % Constant, F(X) same size as X
    %       F = Function(A0, A1, .. An);    % Polynomial function
    %
    %       % Debugging tools:
    %       F = Function('Logger', name);   % see 'Function.logger'
    %       F = Function(...).dectype(X);   % see 'Function.dectype'
    %
    %   User-defined function:
    %       You can provide your own function 'fcn'.
    %
    %       This function will be called by:
    %           fcn(X, ...);
    %       Where the '...' are the same as the ones used in the constructor:
    %           F = Function(fcn, ...);
    %
    %       The function 'fcn' can return:
    %           Y = fcn(X, ...);            % Uncomplete, not recommended
    %           [Y, grad] = fcn(X, ...);    % Scalar function
    %           [Y, DY, DYs] = fcn(X, ...); % General function
    %       The returned arguments must be:
    %           Y       - value of 'fcn' in X.
    %           grad    - gradient of 'fcn' in X, ONLY for scalar functions.
    %           DY      - derivative function of 'fcn' in X, called as DY(dX).
    %           DYs     - adjoint of the derivative in X, called as DYs(dY).
    %
    %   See also:
    %       Function.dectype
    %       Function
    %       CellFunction

        % Empty constructor
        if (~exist('arg', 'var'))

        % Copy constructor Function
        elseif (isa(arg, 'Function'))
            this = arg;

        % Cell of Function
        elseif (iscell(arg) && isa(arg{1}, 'Function'))
            if (any(cellfun(@(F) ~isa(F, 'Function'), arg)))
                error('Invalid cell of Functions');
            elseif (any(cellfun(@(F) ~isempty(F.fcn), arg)))
                this.fcn = @(X) cellfun(@(F) F.eval(X, 1), arg, 'UniformOutput', false);
            else
                this.D = @(X) this.evalCellDerivative(arg, X);
            end;

        % Polynomial function
        elseif (isnumeric(arg))
            this = Function(@Function.fcn_polynom, arg, varargin{:});

        % Functions by name
        elseif (ischar(arg))
            switch lower(arg)
                case '{}'
                    this = Function(@Function.fcn_cellsubsref, varargin{:});
                case '(:)'
                    this = Function(@Function.fcn_reshape, [], 1);
                case 'logger'
                    this = Function(@Function.fcn_logger, varargin{1:min(2,end)});
                case 'linear'
                    this = Function(@Function.fcn_linear, varargin{:});
                case {'id', 'identity'}
                    this = Function(@Function.fcn_identity);
                case 'reshape'
                    this = Function(@Function.fcn_reshape, varargin{:});
                case 'polynom'
                    this = Function(@Function.fcn_polynom, varargin{:});
                otherwise
                    error('Unknown Function keyword ''%s''', arg);
            end;

        % User-defined function
        elseif (isa(arg, 'function_handle'))
            n = nargout(arg);
            classPrefix = strcat(class(this), '.');
            if (exist('arg', 'builtin'))
                error('Cannot use a built-in function: gradient is not provided');
            elseif (n == 1)
                this.fcn = @(X) arg(X, varargin{:});
            elseif (n == 2)
                this.grad = @(X) arg(X, varargin{:});
            elseif (n == 3)
                this.D = @(X) arg(X, varargin{:});
            elseif (strncmpi(func2str(arg), classPrefix, length(classPrefix)))
                % static methods (nargout may be -1)
                this.D = @(X) arg(X, varargin{:});
            else
                error('Cannot use this user function: invalid number of output arguments');
            end;

        % Invalid argument
        else
            error('Invalid argument')

        end;
    end;

    % Get handles to the function or its derivative
    function F = get(this, getValue, getGradient, getDerivative, getDerivStar)
    % GET   - get the function as a handle.
    %
    %   Usage:
    %       h = F.get(value, gradient, derivative, deriv_star);
    %
    %   Examples:
    %
    %       % Elementary handles:
    %       h = F.get();            % h: X -> value(X)
    %       h = F.get(0, 1);        % h: X -> gradient(X)
    %       h = F.get(0, 0, 1);     % h: X -> derivative(X)
    %       h = F.get(0, 0, 0, 1);  % h: X -> deriv_star(X)
    %
    %       % Composite handles:
    %       h = F.get(1, 1);        % h: X -> (value(X), gradient(X))
    %       h = F.get(1, 0, 1, 0);  % h: X -> (value(X), derivative(X))
    %       h = F.get(0, 0, 1, 1);  % h: X -> (derivative(X), deriv_star(X))
    %
    %   Note:
    %       It is more efficient to calling a composite handle once:
    %       >> h = F.get(1, 1);
    %       >> [Y, grad] = h(X);
    %       rather than calling the elementary handles separately:
    %       >> Y = F.value(X);        % or Y=h(X) with h=F.get(1)
    %       >> grad = F.gradient(X);  % or grad=h(X) with h=get(0,1);
    %
    %   See also:
    %       Function.eval
    %       <a href="matlab: properties(Function)">Properties</a>

        % Default arguments
        if (~exist('getValue', 'var')),         getValue = true;        end;
        if (~exist('getGradient', 'var')),      getGradient = false;    end;
        if (~exist('getDerivative', 'var')),    getDerivative = false;  end;
        if (~exist('getDerivStar', 'var')),     getDerivStar = false;   end;

        % Resulting function
        F = @(X) this.eval(X, getValue, getGradient, getDerivative, getDerivStar);
    end;

    % Evaluate the function or its derivative
    function varargout = eval(this, X, getValue, getGradient, getDerivative, getDerivStar)
    % EVAL  - evaluate the function in X.
    %
    %   Usage:
    %       Y = F.eval(X);
    %       [Y, ...] = F.eval(X, value, gradient, derivative, deriv_star);
    %
    %   Examples:
    %
    %       % Elementary:
    %       Y    = F.eval(X);               % Same as F.value(X)
    %       grad = F.eval(X, 0, 1);         % Same as F.gradient(X)
    %       DY   = F.eval(X, 0, 0, 1);      % Same as F.derivative(X)
    %       DYs  = F.eval(X, 0, 0, 0, 1);   % Same as F.deriv_star(X)
    %
    %       % Composite:
    %       [Y, grad] = F.eval(1, 1);
    %       [Y, DY]   = F.eval(1, 0, 1, 0);
    %       [DY, DYs] = F.eval(0, 0, 1, 1);
    %
    %   Note:
    %       It is more efficient to make a composite evaluation once:
    %       >> [Y, grad] = F.eval(X, 1, 1);
    %       rather than to make elementary evaluation separately:
    %       >> Y = F.value(X);  % = F.eval(X, 1);
    %       >> grad = F.gradient(X);  % = F.eval(X, 0, 1);
    %
    %   See also:
    %       Function.get
    %       <a href="matlab: properties(Function)">Properties</a>

        % Default arguments
        if (~exist('getValue', 'var')),         getValue = true;        end;
        if (~exist('getGradient', 'var')),      getGradient = false;    end;
        if (~exist('getDerivative', 'var')),    getDerivative = false;  end;
        if (~exist('getDerivStar', 'var')),     getDerivStar = false;   end;

        % Check number of output
        nget = sum(logical([getValue getGradient getDerivative getDerivStar]));
        if (nget < nargout)
            error('Too many output arguments');
        end;

        % Check input type
        if (Function.CHECK_TYPE_ON_CALL && this.is_typed)
            if (~Function.cellmatch(X, this.type_in))
                error('Invalid input type');
            end;
        end;


        % Evaluate the function
        if (~isempty(this.grad))
            [Y, G] = this.grad(X);
            DFYs = @(dy) Function.cellmap([], @(g) g * dy, G);  % = G * dy;
            DFY = @(dX) Function.cellmap(@sum, @(a,b) a(:) .* b(:), G, dX);  % = sum(G(:) .* dX(:));
        elseif (~isempty(this.D))
            [Y, DFY, DFYs] = this.D(X);
            if (getGradient)
                if(length(Y(:)) > 1)
                    error('Gradient is not available: function is not scalar');
                else
                    G = DFYs(1);
                end;
            end;
        elseif (~isempty(this.fcn))
            Y = this.fcn(X);
            if (getGradient || getDerivative || getDerivStar)
                error('Gradient and derivative are not available');
            end;
        else
            error('Invalid function');
        end;

        % Get gradient, if any
        if (getGradient && ~exist('G', 'var'))
            if (~exist('DFYs', 'var'))
                error('Gradient is not available');
            elseif (length(Y(:)) > 1)
                error('Gradient is not available: function is not scalar');
            else
                G = DFYs(1);
            end;
        end;

        % Assign output
        varargout = {};
        if (getValue),      varargout{end+1} = Y;       end;
        if (getGradient),   varargout{end+1} = G;       end;
        if (getDerivative), varargout{end+1} = DFY;     end;
        if (getDerivStar),  varargout{end+1} = DFYs;    end;

    end;

    % Minimize the scalar function, using quasi-Newton
    function [Xmin, R] = minimize(this, X0, niter, callback)
    % MINIMIZE      - minimize the (scalar) function.
    %
    %   Usage:
    %       Xmin = F.minimize(X0, niter);               % find a minimizer
    %       Xmin = F.minimize(X0, niter, callback);     % called with X, F(Xn) at each iteration
    %       [X, energies] = F.minimize(...);            % also return the values F(Xn)
    %
    %   Algorithm:
    %       The BFGS algorithm is used, see perform_bfgs.

        vec2mat = Function('Reshape', size(X0));
        F = this : vec2mat;

        function value = bfgs_callback(fcn, Xvec, value)
            fcn(vec2mat.value(Xvec), value);
        end

        if (~isscalar(this.value(X0)))
            error('Cannot minimize a non-scalar function');
        end;

        opt = struct('niter', niter);
        if (exist('callback', 'var'))
            opt.report = @(Xvec, value) bfgs_callback(callback, Xvec, value);
        end;

        [X, R] = perform_bfgs(F.get(1, 1), X0(:), opt);
        Xmin = vec2mat.value(X);
    end;

    % Minimize the scalar function, using gradient descent
    function [Xmin, R] = minimizeGrad(this, X0, tau, niter, callback)
    % MINIMIZE GRAD     - minimize the function using gradient descent.
    %
    %   Usage:
    %       Xmin = F.minimizeGrad(X0, tau, niter);
    %       Xmin = F.minimize(X0, tau, niter, callback);
    %       [X, energies] = F.minimize(...);
    %
    %   Algorithm:
    %       Gradient descent with step size = tau.
    %       Convergence if 0 < tau < 2 / ||grad(F)||
    %
    %   See also:
    %       Function.minimize

        Xmin = X0;
        R = zeros(niter, 1);
        h = waitbar(0, 'Minimizing with gradient descent...');
        if (~exist('callback', 'var'))
             callback = @(X, value) 0;
        end;

        for n = 1:niter
            [R(n), dX] = this.eval(Xmin, 1, 1);
            callback(Xmin, R(n));
            waitbar(n / niter, h);
            Xmin = Xmin - tau * dX;
        end;
        close(h);

    end;

    % Debugging function
    function debug(this)  %#ok
    % DEBUG     - launch the debugger to access any members of a Function
    %
    %   Usage:
    %       F.debug();
        keyboard;
    end;

end;

% Tools functions
methods (Access=public)

    % Declare the types of the function
    function this = dectype(this, Xref, Yref, doCheck)
    % DEC TYPE  - declare and check the types involved in the function
    %
    %   Usage:
    %       F = Function(...).dectype(X);
    %       F = Function(...).dectype(X, Y=Fcn.value(X), doCheck=true);
    %       F = G.dectype(...);  % F is a typed version of G
    %       G = F.dectype();  % remove types of F
    %
    %   Arguments:
    %       F       - Function, typed
    %       G       - Function
    %       X       - a sample argument for F
    %       Y       - a sample value of F(X)
    %       doCheck - if false, declare types but do not check them.
    %
    %   Note:
    %       Functions supports numerical arrays and (nested) cells.
    %
    %   Description:
    %       Declare the input/output types of the Function.
    %       This allows for types checking:
    %           1. check the function and derivative type at declaration.
    %           2. check compatibility when composing with other functions.
    %           3. check argument type at function call.
    %       These checks can be disabled:
    %           - by using argument 'doCheck=false'.
    %           - by setting constant 'Function.CHECK_TYPE_ON_CALL=false'.
    %           - by setting constant 'Function.CHECK_TYPE_ON_DECLARE=false'.
    %           - by setting constant 'Function.DISABLE_TYPES=true'.
    %
    %   Examples:
    %       F = Function('id').dectype(0);  % identity on scalars
    %       F = Function('id').dectype([0 0]);  % identity on 1x2 arrays
    %       F = Function('id').dectype({0 0});  % identity on 1x2 cells
    %       F = Function('id').dectype({{0}});  % identity on 2 nested cells
    %       F = Function('id').dectype(0, {0});  % error: types mismatch!
    %
    %       % Declare types without calling the function:
    %       F = VerySlowFcn.dectype(0, [0 0], false);
    %
    %       % Compose functions
    %       F = Function('id').dectype(0);
    %       G = Function('id').dectype({0});
    %       First = Function('{}', 1);
    %       H = F.o(F);  % ok
    %       H = F.o(G);  % error: types mismatch
    %       H = F.o(First).o(G);  % ok (type of 'First' is infered from G)
    %
    %   See also:
    %       Function.is_typed
    %       Function.type_in
    %       Function.type_out
    %       Function.Function
    %       Function.check
    %       Function.cellmatch
    %       Function.CHECK_TYPE_ON_CALL

        % No argument: removes types
        if (~exist('Xref', 'var') || Function.DISABLE_TYPES)
            this.types = [];
            return;
        end;

        % No check: only stores types
        if (exist('doCheck', 'var') && ~doCheck  ||  ~Function.CHECK_TYPE_ON_DECLARE)
            if (exist('Yref', 'var') && ~isempty(Yref))
                this.types = {Xref, Yref};
            else
                this.types = {Xref, this.eval(Xref)};
            end;
            return;
        end;

        % Get and check output type
        [Y, DY, DYs] = this.eval(Xref, 1, 0, 1, 1);
        if (~exist('Yref', 'var'))
            Yref = Y;
        elseif (~Function.cellmatch(Yref, Y))
            error('Function output type mismatch');
        end;

        % Check derivative types
        if (~Function.cellmatch(DY(Xref), Yref))
            error('Function derivative type mismatch');
        elseif (~Function.cellmatch(DYs(Yref), Xref))
            error('Function derivative adjoint type mismatch');
        end;

        % Store types
        this.types = {Xref, Yref};
    end;

    % Check whether derivative is correct
    function [bool, errstr] = check(this, Xref, n)
    % CHECK     - check whether derivative and adjoint are correct
    %
    %   Usage:
    %       bool = F.check(Xref, n=1);
    %       bool = F.check();  % ONLY if F is typed (see 'dectype')
    %       [bool, errstr] = F.check(...);
    %
    %   Arguments:
    %       Xref    - a variable X (to get its dimensions)
    %                default=type_in
    %       n = 1   - perform n tests
    %
    %   Returns:
    %       bool    - true iif the derivative or its adjoint is wrong
    %       errstr  - if bool is false, cell of strings describing problems
    %
    %   Note:
    %       Currently, only support Xref being an array
    %
    %   See also:
    %       Function.dectype

        h = [1e-4 1e-6];   % to check differential: h(1) is tolerance, h(2) is small step
        epsilon = 1e-9;    % to check equality

        % Get Xref from type
        if (~exist('Xref', 'var'))
            Xref = this.type_in;
        end;

        % Only support arrays yet
        if (~isnumeric(Xref))
            error('Function checking only supports numerical array yet');
        end;

        % What if error
        errstr = {};
        bool = true;
        function setError(msg)
            errstr{end+1} = msg;
            bool = false;
        end

        % Initializations
        dotp = @(x,y) x(:)' * y(:);
        normLp = @(x) sqrt(mean(x(:).^2)); % or: mean(abs(x(:)));
        if (~exist('n', 'var'))
            n = 1;
        end;

        % Cells functions
        randofsize = @(x) rand(size(x)); % @(X) Function.cellmap([], @(x) rand(size(x)), X);
        maxerr = @(x) max(abs(x(:))); % @(X) Function.cellmap(@max, @abs, X);

        % Perform n tests
        for k = 1:n

            % Generate points
            X = randofsize(Xref);
            [Y, DY, DYs] = this.eval(X, 1, 0, 1, 1);
            dX = randofsize(X);
            dY = randofsize(Y);
            DY_dX = DY(dX);
            DYs_dY = DYs(dY);

            % Check for size
            if (~isequal(size(DY_dX), size(Y)))
                setError('Differential returns wrong dimensions');
            end;
            if (~isequal(size(DYs_dY), size(X)))
                setError('Differential adjoint returns wrong dimensions');
            end;

            % Check for linearity and adjunction
            if (~isempty(this.D))
                a = rand();
                dXbis = randofsize(X);
                dYbis = randofsize(Y);
                errLX = maxerr(a *  DY_dX +  DY(dXbis) -  DY(a*dX+dXbis));
                errLY = maxerr(a * DYs_dY + DYs(dYbis) - DYs(a*dY+dYbis));
                if (max(errLX, errLY) > epsilon)
                    setError('The differential or its adjoint is not linear');
                end;
                errAdj = dotp(DY_dX, dY) - dotp(dX, DYs_dY);
                if (abs(errAdj) > epsilon)
                    setError('Differential adjoint is wrong (not adjoint)');
                end;
            end;

            % Check for derivative
            c = h(end) / normLp(dX);
            finite_diff = this.eval(X + c * dX, 1) - this.eval(X - c * dX, 1);
            approx_err = normLp(finite_diff / (2 * c) - DY_dX);
            if (approx_err / normLp(Y) > h(1))
                setError('Differential seems to be wrong');
            end;

            % Display them
            if (false)
                log10c = -12:12;
                co = 1 / normLp(dX);
                err = zeros(size(log10c));
                for p = 1:length(log10c)
                    cc = 10^log10c(p) * co;
                    approx_2DY_dX = this.eval(X + cc * dX, 1) - this.eval(X - cc * dX, 1);
                    err(p) = normLp(approx_2DY_dX / (2 * cc) - DY_dX);
                end;
                semilogy(log10c, err / normLp(Y), '.-b', log10(c), h(1), 'or');
                axis tight;
                title('Approximation error of the differential');
                xlabel('Step size for differential estimation (log)');
            end;

            % Stop if error
            if (~bool)
                return;
            end;
        end;
    end;

    % Check wether the function is linear
    function [bool, F_star] = isLinear(this, Xref)
    % IS LINEAR     - check whether the function is linear
    %
    %   Usage:
    %       bool = F.isLinear(Xref);
    %       [bool, F_star] = F.isLinear(Xref);
    %       ... = F.isLinear();  % ONLY if F is typed (see 'dectype')
    %
    %   Arguments:
    %       F       - a function
    %       Xref    - a variable X (to get its dimensions)
    %
    %   Returns:
    %       bool    - true iff F is linear
    %       F_star  - if F is linear, adjoint Function of F
    %
    %   Note:
    %       F is linear  iff  F = DF(X) for any X.
    %       In that case, its adjoint is DFs(X).
    %
    %   See also:
    %       Function.getAdjoint
    %       Function.dectype
    %       Function.check

        epsilon = 1e-9;    % to check equality

        % Get Xref from type
        if (~exist('Xref', 'var'))
            Xref = this.type_in;
        end;

        % Useful functions
        maxerr = @(X,Y) Function.cellmap(@max, @(x,y) abs(x-y), X, Y);
        randofsize = @(X) Function.cellmap([], @(x) rand(size(x)), X);

        % Get handles to derivative and adjoint
        [DF, DFs] = this.eval(randofsize(Xref), 0, 0, 1, 1);

        % Check equality of F and DF
        X = randofsize(Xref);
        bool = (maxerr(this.eval(X), DF(X)) <= epsilon);

        % Adjoint function
        if (~bool || nargout < 2)
            F_star = [];
        elseif (~this.is_typed)
            F_star = Function('Linear', DFs, DF);
        else
            F_star = Function('Linear', DFs, DF).dectype(this.type_out);
        end;
    end;

    % Get the linear adjoint, if linear
    function F_star = getAdjoint(this, Xref)
    % GET ADJOINT   - get the adjoint function of a linear function
    %
    %   Usage:
    %       F_star = F.getAdjoint(Xref);
    %       F_star = F.getAdjoint();  % ONLY if F is typed (see 'dectype')
    %
    %   Arguments:
    %       F       - a linear Function
    %       Xref    - a variable X (to get its dimensions)
    %
    %   Returns:
    %       F_star  - the adjoint Function of F
    %
    %   Error:
    %       If F is not linear
    %
    %   See also:
    %       Function.isLinear

        % Get Xref from type
        if (~exist('Xref', 'var'))
            Xref = this.type_in;
        end;

        % Get adjoint
        [isLin, F_star] = this.isLinear(Xref);
        if (~isLin)
            error('Cannot get the Adjoint: the Function is not linear');
        end;
    end;

end;

% Tools functions
methods (Static)

    % Get / reset the logger
    function out = logger(reset)
    % LOGGER    - get / reset the logger content
    %
    %   Usage:
    %       logs = Function.logger(reset=false);
    %
    %   Returns:
    %       logs                - all the logs, as a structure of structure
    %       logs.(name).(what)  - cell containing all the evaluation points
    %                               'name' is the name of the logger
    %                               'what' describes what is evaluated
    %
    %   Logger Function:
    %       A logger function is creating with:
    %       >> Id = Function('Logger', name);  % logging function
    %       It is an identity function which records each of its call.
    %       A user-define function can be applied at logging:
    %       >> Id = Function('Logger', name, fcn);
    %       This function also logs 'fcn(X)' for each X it is called on.
    %
    %   Examples:
    %
    %       % Create a logger called 'F':
    %       Logger = Function('Logger', 'F', @num2str);
    %
    %       % Call it, and call its derivative:
    %       x = Logger.value(42);  % compute 1 value
    %       [y, D] = Logger.eval(3.14, 1, 0, 1);  % 1 value and 1 derivative
    %       dy = D(1);
    %       Ds = Logger.deriv_star(1337);  % require 1 value computation
    %       grad = Ds(1);
    %       steepest = Ds(-1);
    %
    %       % Content of the logger F:
    %       disp(Function.logger.F)
    %
    %       % Get and reset all the logs:
    %       logs = Function.logger(true);
    %
    %
    %
    %   See also:
    %       Function.Function

        out = Function.fcn_logger('get');
        if (exist('reset', 'var') && reset)
            Function.fcn_logger('reset');
        end;
    end;

    % Apply a function of arrays to a cells' hierarchy
    function [varargout] = cellmap(reducefcn, mapfcn, varargin)
    % CELL MAP  - apply a function of arrays to a cells' hierarchy
    %
    %   Usage:
    %       scalar = Function.cellmap(reducefcn, [], Y);
    %       scalar = Function.cellmap(reducefcn, mapfcn, X, ...);
    %       [Y, ...] = Function.cellmap([], mapfcn, X, ...);
    %
    %   Arguments:
    %       reducefcn   - function F: vector --> scalar
    %                     must satisfy F( [X; Y] ) = F( [F(X); F(Y)] )
    %       mapfcn      - function: x, ... --> y, ...
    %       X           - a cell hierarchy
    %
    %   Returns:
    %       scalar      - a scalar
    %       Y           - a cell hierarchy, same hierarchy as the X's
    %                     made of the (y, ...) = f(x, ...) for each x in X
    %
    %   Details:
    %       This is a recursive extension of cellfun to cells' hierarchies.
    %       The X's are cells' hierarchies of arguments x's for mapfcn.
    %       The Y's are cells' hierarchies of returned y's from mapfcn.
    %       The scalar is the result of reducefcn applied to all y in Y.
    %       To chain map and reduce, mapfcn must return a single y.
    %
    %   Examples:
    %
    %       % Create a sample cell hierarchy:
    %       imget = @(name) double(imread(name)) / 255;
    %       A = { {1, 2}, [3 4 5]'; {{{pi}}}, {imget('lena.png')} };
    %
    %       % Reduce: get the maximum among all value:
    %       m = Function.cellmap(@max, [], A);
    %
    %       % Map: compute the element-wise exponential:
    %       E = Function.cellmap([], @exp, A);
    %
    %       % Map: generate random values with same hierarchy:
    %       B = Function.cellmap([], @(x) rand(size(x)), A);
    %
    %       % Map: element-wise product:
    %       AB = Function.cellmap([], @times, A, B);
    %
    %       % Map: element-wise sum and difference:
    %       sumdif = @(x,y) deal(x+y, x-y);  % usage: [s,d] = sumdif(a, b);
    %       [S,D] = Function.cellmap([], sumdif, A, B);
    %
    %       % Map-reduce: compute the L-2 norm of A:
    %       v = Function.cellmap(@sum, @(x) x.^2, A);
    %
    %       % Map-reduce: compute the L-inf distance between A and B:
    %       d = Function.cellmap(@max, @(x,y) abs(x-y), A, B);
    %
    %   See also:
    %       Function.cellmatch

        varargout = cell(1, max(1, nargout));  % at least 1 output
        if any(cellfun(@iscell, varargin))
            % Recursive call
            recfcn = @(varargin) Function.cellmap(reducefcn, mapfcn, varargin{:});
            [varargout{:}] = cellfun(recfcn, varargin{:}, 'UniformOutput', ~isempty(reducefcn));
        elseif ~isempty(mapfcn)
            % Final map call
            [varargout{:}] = mapfcn(varargin{:});
        else
            % Final call, no map
            varargout = varargin;
        end;

        % Reduce
        if ~isempty(reducefcn)
            varargout = { reducefcn(varargout{:}(:)) };
        end;

    end;

    % Check whether cells has the same hierarchy and size
    function bool = cellmatch(C, varargin)
    % CELL MATCH    - compare size and structure of cell hierarchies
    %
    %   Usage:
    %       bool = Function.cellmatch(C, D);
    %       bool = Function.cellmatch(C, D, ...);
    %
    %   Arguments:
    %       C, D, ...   - cell hierarchies
    %
    %   Returns:
    %       True iff all the cell has the same hierarchy and are numerical
    %
    %   Examples:
    %       A = ones(4, 2);
    %
    %       % With arrays:
    %       Function.cellmatch(true(1,5))  % false: not a numerical type
    %       Function.cellmatch(zeros(1,5), 0:4)  % true: same size
    %       Function.cellmatch(zeros(5,1), 0:4)  % false: transposed
    %       Function.cellmatch(A, rand(size(A)), A(:))  % false: since A(:)
    %       Function.cellmatch(A, rand(size(A)), zeros(size(A)))  % true
    %
    %       % With simple cells:
    %       Function.cellmatch(A, {A})  % false: array vs. cell
    %       Function.cellmatch({A}, {{A}})  % false: cell vs. cell of cell
    %       Function.cellmatch({1 2}, {3 A})  % false: because 2 vs. A
    %       Function.cellmatch({{1 2}}, {{3 4}})  % true
    %       Function.cellmatch({{1 2}}, {{3;4}})  % false: not same shape
    %
    %       % Nested cells:
    %       C = {pi, 123; {{rand(size(A))}}, { 2:3 ; 1:5}}
    %       D = {42, 666; {{ones(size(A))}}, {[1 0]; ones(1,5)}}
    %       Function.cellmatch(C, D)   % true: same hierarchy
    %       Function.cellmatch(C, D')  % false: C{1,2} scalar, not D{2,1}
    %
    %   See also:
    %       Function.cellmap

        % Check whether all arguments are array of same size
        areCells = cellfun(@iscell, varargin);
        areNumeric = isnumeric(C) && all(cellfun(@isnumeric, varargin));
        allSameSize = all(cellfun(@(x) isequal(size(C), size(x)), varargin));
        bool = ~iscell(C) && ~any(areCells) && allSameSize && areNumeric;

        % In case all are cells of the same size, recursive check
        if (iscell(C) && all(areCells) && allSameSize)
            recfcn = @(varargin) Function.cellmatch(varargin{:});
            boolsRec = all(cellfun(recfcn, C, varargin{:}));
            bool = all(boolsRec(:));
        end;
    end;

end;

% Main operators (as methods)
methods (Access=public)

    % Constant multiplication and addition
    function G = x(F, a, b)
    % X     - affine adjustment, G = F*a + b
    %
    %   Usage:
    %       G = F.x(a);
    %       G = F.x(a, b);
    %       G = F.x({u v}, b);
    %
    %   Argument:
    %       a   - multiplicative constant
    %       b   - additive constant
    %       u   - left multiplicative constant
    %       v   - right multiplicative constant
    %       F   - input Function
    %       G   - output Function, defined as:
    %               G: X ->     F(X) * a + b
    %               G: X -> u * F(X) * v + b

        if (~exist('b', 'var'))
            b = 0;
        end;

        % Deal with left/right multiplication
        if (~iscell(a))
            [u,v] = deal(1, a);
        elseif (length(a) == 2)
            [u,v] = deal(a{:});
        else
            error('Cell must contain exactly 2 elements');
        end;

        % Define output function
        G = Function();
        allscalar = isscalar(u) && isscalar(v) && isscalar(b);
        if (~isempty(F.fcn))
            G.fcn = @(X) u * F.value(X) * v + b;
        elseif (~isempty(G.grad) && allscalar)
            G.grad = F.evalStretchedGradient(u * v, b, X);
        else
            G.D = @(X) F.evalStretchedDerivative(u, v, b, X);
        end;
    end;

    % Composition
    function FoG = o(F, G)
    % O     - function composition (aka colon ':' operator)
    %
    %   Usage:
    %       FoG = F.o(G);

        FoG = Function();
        [Ft, Gt] = deal(Function(F), Function(G));  % typed version
        [F, G] = deal(Ft.dectype(), Gt.dectype());  % non-typed version

        % Compose functions
        if (~isempty(F.fcn) || ~isempty(G.fcn))
            FoG.fcn = @(X) F.value(G.value(X));
        elseif (~isempty(F.grad))
            FoG.grad = @(X) F.evalComposedGradient(G, X);
        else
            FoG.D = @(X) F.evalComposedDerivative(G, X);
        end;

        % Deal with types
        if (~Function.DISABLE_TYPES && Gt.is_typed)
            if (~Ft.is_typed)
                Ft = Ft.dectype(Gt.type_out);
            elseif (Function.CHECK_TYPE_ON_DECLARE && ~Function.cellmatch(Ft.type_in, Gt.type_out))
                error('Cannot compose functions, incompatible types');
            end;
            FoG = FoG.dectype(Gt.type_in, Ft.type_out, false);
        end;
    end;

end;

% Main operators (overloaded)
methods (Access=public)

    % Sum of 2 functions (of the same variable)
    function F = plus(F1, F2)
    % PLUS (+)      - sum of Functions of the same variable
    %
    %   Usage:
    %       H = F + G
    %       H = F + arg
    %       H = F + ...
    %
    %   Inputs:
    %       F       - Function
    %       G       - Function
    %       arg     - an argument to build G = Function(arg)
    %       ...     - any number of arguments like 'G' or 'arg'
    %
    %   Returns:
    %       H       - Function H:  X --> F(X) + G(X) + ...

        F = Function();
        F1 = Function(F1);
        F2 = Function(F2);
        if (~isempty(F1.fcn) || ~isempty(F2.fcn))
            F.fcn = @(X) F1.value(X) + F2.value(X);
        elseif (~isempty(F1.grad) || ~isempty(F2.grad))
            F.grad = @(X) F1.evalSumGradient(1, F2, X);
        else
            F.D = @(X) F1.evalSumDerivative(1, F2, X);
        end;
    end;

    % Difference of 2 functions (of the same variable)
    function F = minus(F1, F2)
    % MINUS (-)    	- difference between Functions of the same variable
    %
    %   Usage:
    %       (similar to opeartor plus)
    %
    %   See also:
    %       Function.plus
    %       Function.x

        F = Function();
        F1 = Function(F1);
        F2 = Function(F2);
        if (~isempty(F1.fcn) || ~isempty(F2.fcn))
            F.fcn = @(X) F1.value(X) - F2.value(X);
        elseif (~isempty(F1.grad) || ~isempty(F2.grad))
            F.grad = @(X) F1.evalSumGradient(-1, F2, X);
        else
            F.D = @(X) F1.evalSumDerivative(-1, F2, X);
        end;
    end;

    % Opposite of a function
    function G = uminus(F)
    % UMINUS (-)        - additive inverse of a number
    %
    %   Usage:
    %       H = -F;
    %
    %   Input:
    %       F   - Function
    %
    %   Output:
    %       H   - Function H:  X --> -F(X)
    %
    %   See also:
    %       Function.x

        G = Function(F).x(-1);
    end;

    % Product of 2 functions (of the same variable)
    function F = mtimes(F1, F2)
    % MTIMES (*)        - product between Functions of the same variable
    %
    %   Usage:
    %       (similar to opeartor plus)
    %
    %   See also:
    %       Function.plus
    %       Function.x

        F = Function();
        F1 = Function(F1);
        F2 = Function(F2);
        if (~isempty(F1.fcn) || ~isempty(F2.fcn))
            F.fcn = @(X) F1.value(X) * F2.value(X);
        elseif (~isempty(F1.grad) || ~isempty(F2.grad))
            F.grad = @(X) F1.evalProdGradient(F2, X);
        else
            F.D = @(X) F1.evalProdDerivative(F2, X);
        end;
    end;

    % Composition of functions
    function FoG = colon(F, G, varargin)
    % COLON (:)        - composition of Functions
    %
    %   Warning:
    %       Beware of Matlab's operator predecence:
    %       >> Fcn = E+F : G*H;     % composition of E+F and G*H
    %       >> Evil = E + F:G * H;  % bad notation, still equals (E+F) : (G*H)
    %       Alternative notation, to avoid mistakes:
    %       >> Fcn = (E+F).o(G*H);      % same as previous 'Fcn'
    %       >> Other = E + F.o(G) * H;  % different from 'Evil'
    %
    %   Usage:
    %       (similar to opeartor plus)
    %
    %   See also:
    %       Function.plus
    %       Function.o

        FoG = Function(F).o(G);
        if (~isempty(varargin))
            FoG = FoG.colon(varargin{:});
        end;
    end;

end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Private properties
properties (Access=private)
    fcn;    % Y = @(X) ...
    grad;   % [Y, grad] = @(X) ...
    D;      % [Y, DFY, DFYs] = @(X) ...
    types;  % Sample pair {X, Y} for type checking
end;

% Private methods
methods (Access=private)

    % Evaluate the derivative of {F, ...} in X
    function [Y, DY, DYs] = evalCellDerivative(osef, C, X)  %#ok
        cellfcn = @(fcn, cell) cellfun(fcn, cell, 'UniformOutput', false);
        cellfcn2 = @(fcn, cell, args) cellfun(fcn, cell, args, 'UniformOutput', false);

        sumargs = @(A, varargin) sum(cat(ndims(A)+1, A, varargin{:}), ndims(A)+1);
        cellsum = @(cell) Function.cellmap([], sumargs, cell{:});
        %dd = ndims(X) + 1;
        %cellsum = @(cell) sum(cat(dd, cell{:}), dd);
        [Y, CDY, CDYs] = cellfcn(@(F) F.eval(X, 1, 0, 1, 1), C);
        DY = @(dX) cellfcn(@(F) F(dX), CDY);  % = { DY{k}(dX) for all k }
        DYs = @(dY) cellsum(cellfcn2(@(F,y) F(y), CDYs, dY));  % = sum( CDYs{k}(dY{k}) for all k }
    end;

    % Evaluate the gradient of a*F + b in X
    function [z, grad] = evalStretchedGradient(F, a, b, X)
        [y, g] = F.eval(X, 1, 1);
        z = a * y + b;
        grad = a * g;
    end;

    % Evaluate the derivative of u*F*v + b in X
    function [Z, DZ, DZs] = evalStretchedDerivative(F, u, v, b, X)
        [Y, DY, DYs] = F.eval(X, 1, 0, 1, 1);
        Z = u * Y * v + b;
        DZ = @(dX) u * DY(dX) * v;
        DZs = @(dY) DYs(u' * dY * v') ;
    end;

    % Evaluate the gradient of F1 * F2 in X
    function [y, grad] = evalProdGradient(F1, F2, X)
        [y1, g1] = F1.eval(X, 1, 1);
        [y2, g2] = F2.eval(X, 1, 1);
        y = y1 * y2;
        grad = g1 * y2 + y1 * g2;
    end;

    % Evaluate the derivative of F1 * F2 in X
    function [Y, DY, DYs] = evalProdDerivative(F1, F2, X)
        [Y1, D1, Ds1] = F1.eval(X, 1, 0, 1, 1);
        [Y2, D2, Ds2] = F2.eval(X, 1, 0, 1, 1);
        Y = Y1 * Y2;
        tr = @(U) Function.cellmap([], @transpose, U);
        ssum = @(U, V) Function.cellmap([], @(u,v) u + v, U, V);
        DY = @(dX) ssum(D1(dX) * Y2, Y1 * D2(dX));  % = D1(dX)*Y2 + Y1*D2(dX)
        DYs = @(dY) ssum(Ds1(dY * tr(Y2)), Ds2(tr(Y1) * dY));  % = Ds1(dY*Y2') + Ds2(Y1',dY)
    end;

    % Evaluate the gradient of F1 + a * F2 in X
    function [y, grad] = evalSumGradient(F1, a, F2, X)
        [y1, g1] = F1.eval(X, 1, 1);
        [y2, g2] = F2.eval(X, 1, 1);
        wsum = @(U, V) Function.cellmap([], @(u,v) u + a*v, U, V);
        y = wsum(y1, y2);  % = y1 + a * y2;
        grad = wsum(g1, g2);
    end;

    % Evaluate the derivative of F1 + a * F2 in X
    function [Y, DY, DYs] = evalSumDerivative(F1, a, F2, X)
        [Y1, D1, Ds1] = F1.eval(X, 1, 0, 1, 1);
        [Y2, D2, Ds2] = F2.eval(X, 1, 0, 1, 1);
        wsum = @(U, V) Function.cellmap([], @(u,v) u + a*v, U, V);
        Y = wsum(Y1, Y2);  % = Y1 + a * Y2;
        DY = @(dX) wsum(D1(dX), D2(dX));  % = D1(dX) + a * D2(dX);
        DYs = @(dY) wsum(Ds1(dY), Ds2(dY));  % = Ds1(dY) + a * Ds2(dY);
    end;

    % Evaluate the gradient of FoG in X
    function [y, grad] = evalComposedGradient(F, G, X)
        [GX, DGXs] = G.eval(X, 1, 0, 0, 1);
        [y, gradF] = F.eval(GX, 1, 1);
        grad = DGXs(gradF);
    end;

    % Evaluate the derivative of FoG in X
    function [Y, DY, DYs] = evalComposedDerivative(F, G, X)
        [GX, DGX, DGXs] = G.eval(X, 1, 0, 1, 1);
        [Y, DFGX, DFGXs] = F.eval(GX, 1, 0, 1, 1);
        DY = @(dX) DFGX(DGX(dX));
        DYs = @(dY) DGXs(DFGXs(dY));
    end;

end;

% Getters implementation
methods

    function out = get.value(this)
        out = this.get();
    end;

    function out = get.gradient(this)
        out = this.get(0, 1);
    end;

    function out = get.derivative(this)
        out = this.get(0, 0, 1);
    end;

    function out = get.deriv_star(this)
        out = this.get(0, 0, 0, 1);
    end;

    function out = get.is_typed(this)
        out = ~isempty(this.types);
    end;

    function out = get.type_in(this)
        if (isempty(this.types))
            error('No type declared');
        else
            out = this.types{1};
        end;
    end;

    function out = get.type_out(this)
        if (isempty(this.types))
            error('No type declared');
        else
            out = this.types{2};
        end;
    end;

end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% List of functions, available through the constructor
methods (Static, Hidden=true)

    % Cell indexing Function
    function [Y, DY, DYs] = fcn_cellsubsref(X, varargin)
        S = struct('type', '{}', 'subs', {varargin});
        Y = subsref(X, S);  % Alt: Y = X{varargin{:}}
        DY = @(dX) subsref(dX, S);

        % Adjoint function
        Z = Function.cellmap([], @(x) zeros(size(x)), X);
        DYs = @(dY) subsasgn(Z, S, dY);  % return result, Z unchanged

    end;

    % Identity function
    function [Y, DY, DYs] = fcn_identity(X)
        Y = X;
        DY = @(dX) dX;
        DYs = @(dY) dY;
    end;

    % Linear operator
    function [Y, DY, DYs] = fcn_linear(X, L, Ls)
        if (isa(L, 'function_handle') && isa(Ls, 'function_handle'))
            Y = L(X);
            DY = @(dX) L(dX);
            DYs = @(dY) Ls(dY);
        elseif (isnumeric(L) && ~exist('Ls', 'var'))
            Y = L * X;
            DY = @(dX) L * dX;
            DYs = @(dY) L' * dY;
        else
            error('Invalid arguments for linear function');
        end;
    end;

    % Matlab reshape function
    function [Y, DY, DYs] = fcn_reshape(X, varargin)
        Y = reshape(X, varargin{:});
        DY = @(dX) reshape(dX, size(Y));
        DYs = @(dY) reshape(dY, size(X));
    end

    % Polynomial function
    function [Y, DY, DYs] = fcn_polynom(X, A0, varargin)
        repLike = @(x, C) Function.cellmap([], @(c) x * ones(size(c)), C);
        if (isempty(varargin))
            Y = A0;
            DY_value = repLike(0, Y);
            DYs_value = repLike(0, X);
            DY = @(dX) DY_value;
            DYs = @(dY) DYs_value;
        elseif (length(varargin) == 1 && varargin{1} == 0)
            Y = repLike(A0, X);
            D_value = repLike(0, Y);
            DY = @(dX) D_value;
            DYs = @(dY) D_value;
        elseif (~isscalar(X))
            error('Polynom function is not implemented for matrices');
        else
            Y = A0;
            DD = 0;
            for k = 1 : length(varargin)
                Y = Y + varargin{k} * X ^ k;
                DD = DD + k * varargin{k} * X ^ (k-1);
            end;
            DY = @(dX) DD * dX;
            DYs = @(dY) sum(DD(:) .* dY(:));
        end;
    end;

    % A logging function
    function [Y, DY, DYs] = fcn_logger(X, name, userfcn)
        persistent logged;

        % If no argument, return the logs
        if (nargin == 1)
            switch lower(X)
                case 'get'
                    if (isempty(logged))
                        Y = struct();
                    else
                        Y = logged;
                    end;
                case 'reset'
                    logged = [];
                    Y = [];
                otherwise
                    error('Logger: missing name');
            end;
            [DY, DYs] = deal(@() error('Invalid call'));
            return;
        end;

        % Create missing fields
        if (isempty(logged))
            logged = struct();
        end;
        if (~isfield(logged, name))
            logged.(name) = struct();
        end;

        % Logging function
        function out = logAndReturn(value, str, varargin)
            out = value;
            if (~isfield(logged.(name), str))
                logged.(name).(str) = {};
            end;
            logged.(name).(str){end+1} = value;
            if (~isempty(varargin))
                logAndReturn(varargin{:});
            end;
        end

        % Eval function, define derivative
        if (exist('userfcn', 'var'))
            Y = logAndReturn(X, 'value', userfcn(X), 'user_fcn');
        else
            Y = logAndReturn(X, 'value');
        end;
        DY = @(dX) logAndReturn(dX, 'derivative', X, 'derivative_at');
        DYs = @(dY) logAndReturn(dY, 'deriv_adjoint', X, 'deriv_adjoint_at');
    end

end;

end
