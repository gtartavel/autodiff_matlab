function C = ldivide(A, B)
% LDIVIDE (.\)   - element-wise (left) division between cells, or between a cell and a scalar.
%
%   Usage:
%       C = A .\ B;  % A or B may be a scalar
%       C = ldivide(A, B);
   C = cellscalarop({@ldivide A B});
end
