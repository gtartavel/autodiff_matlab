function C = gt(A, B)
% GT (>)    - element-wise '>' between cells, or between a cell and a scalar.
%
%   Usage:
%       C = A > B;  % A or B may be a scalar
%       C = (A > B);
%       C = gt(A, B);
    C = cellscalarop({@gt A B});
end
