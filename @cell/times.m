function C = times(A, B)
% TIMES (.*)   - element-wise multiplication between cells, or between a cell and a scalar.
%
%   Usage:
%       C = A .* B;  % A or B may be a scalar
%       C = times(A, B);
   C = cellscalarop({@times A B});
end
