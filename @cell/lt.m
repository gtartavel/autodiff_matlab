function C = lt(A, B)
% LT (<)    - element-wise '<' between cells, or between a cell and a scalar.
%
%   Usage:
%       C = A < B;  % A or B may be a scalar
%       C = (A < B);
%       C = lt(A, B);
    C = cellscalarop({@lt A B});
end
