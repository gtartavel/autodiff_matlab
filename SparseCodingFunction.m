function X = SparseCodingFunction(Xref, GradF, lambda, tau, niter, useFista, Logger)
% SPARSE CODING FUNCTION    - return a function to compute sparse coding
%
%   Usage:
%       X = SparseCodingFunction(X0, GradF, lambda, tau, niter);
%       X = SparseCodingFunction(... niter, useFista=true, Logger=[]);
%
%   Return:
%       X       - Function  y --> arg min_x  F(y,x) + lambda * ||x||_1
%
%   Arguments:
%       Xref        - a variable X (to get its dimensions)
%       GradF       - Function {y, x, ...} --> Grad_x F(y, x)
%                     y: global parameter, X is also a function of y
%                     x: local parameter of F, to be optimized
%       lambda      - regularization parameter
%       tau         - step size, choose 1/c for GradF c-Lipschitz
%       niter       - number of iteration
%       useFista    - use the Fast version of ISTA algorithm (default=true)
%       Logger      - Function of {y, x, ...} for logging -- see Function.logger
%
%   Examples:
%           Let us implement a sparse-coding into a tight wavelet frame.
%       Assume the wavelet and inverse (adjoint) transform are defined as:
%       >> [wt, iwt] = waveletTransformFunctions(...);
%       Let us define the Function associated to them:
%       >> WT = Function('Linear', wt, iwt);
%       >> IWT = Function('Linear', iwt, wt);  % or call WT.getAdjoint
%           Let us define X: y -> argmin_x 1/2*||iwt(x) - y||^2 + s*||x||_1
%       The fidelity function is:  F(y,x) = ||iwt(x) - y||^2.
%       Its gradient is thus:
%       >> GradF = WT.o(IWT.(getX) - getY);  % Function of {y, x}
%       with:
%       >> GetX = Function('{}', 2);  % x is the 2nd argument
%       >> GetY = Function('{}', 1);  % y is the 1st argument
%       Get the sparse-coding function X for s=1/5 and 50 iterations:
%       >> SparseCode = SparseCodingFcn(GradF, 1/5, 1, 50);
%       Which can be called like this:
%       >> y = rand(240, 320)
%       >> [x, Dx] = SparseCode.eval(y, 1, 0, 1);  % get value & derivative

    % Some functions
    Id = Function('id');
    Fst = Function('{}', 1);
    Snd = Function('{}', 2);
    Trd = Function('{}', 3);
    %Sparse = Function('Linear', @sparse, @full);
    %Full = Function('Linear', @full, @sparse);

    % Iteration step: {Id, X} --> Prox o ( Id - tau * Grad(Id, X) )
    ProxStep = Function(@softThresholding, tau * lambda);
    GradStep = Snd - GradF.x(tau);
    if (~exist('Logger', 'var') || isempty(Logger))
        Step = ProxStep : GradStep;  % Sparse
    elseif (isa(Logger, 'Function'))
        Step = Snd.o(Logger).o({Fst, ProxStep : GradStep});  % Sparse
    else
        error('Invalid logger for sparse coding function');
    end;

    % Algorithms steps
    IstaStep = Function({Fst, Step});        % {Id,    X} --> {Id,    X'}
    FistaStep = Function({Fst, Trd, Step});  % {Id, x, X} --> {Id, X, X'}
    FistaAux = @(w) Function({Fst, Trd.x(1+w) - Snd.x(w), Trd});

    % Apply algorithm
    Zero = Function(0, 0) : Function('polynom', Xref);  % zeros of size of Xref
    if (exist('useFista', 'var') && ~useFista)
        IX = {Id, Zero};
        for n = 1:niter-1
            IX = IstaStep.o(IX);
        end;
        X = Step.o(IX);  % Full
    else
        IxX = {Id, Zero, Zero};
        for n = 1:niter-1
            AuxStep = FistaAux((n-2)/(n+1));
            IxX = AuxStep.o(FistaStep).o(IxX);
        end;
        X = Step.o(IxX);  % Full
    end;
end

% ISTA step:
%   X(Y) <-- prox(tau * lambda) o (Id - tau * gradF(Id, Y)) o X(Y)
%   X    <-- ST o G o X
% FISTA step:
%   Xt    <-- (1+w)*Xn - w*X    % X t.ilde at step k
%   X     <-- Xn                % X at step k
%   Xn    <-- ST o G o Xt       % X at n.ext step k+1
