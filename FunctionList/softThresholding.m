function [Xt, DXt, DXts] = softThresholding(X, T)
% SOFT THRESHOLDING     - soft thresholing function
%
%   Usage:
%       Xt = softThresholding(X, T);
%       [Xt, DXt, DXts] = softThresholding(X, T);

    Xt = X .* max(0, 1 - T ./ abs(X));
    DXt = @(dX) dX .* (Xt ~= 0);
    DXts = DXt;

end