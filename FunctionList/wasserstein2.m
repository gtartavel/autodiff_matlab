function [d, D, Ds] = wasserstein2(X, Y, normalized)
% WASSERSTEIN 2   - squared L2 Wasserstein distance, column-wise.
%
% 	Usage:
%   	d = wasserstein2(X, Y);
%   	d = wasserstein2(X, Y, true);
%       [d, D, Ds] = wasserstein2(...);
%
%   Return:
%       If X and Y are vectors, d is scalar, d = min 1/2 |X-Ys|^2
%           where the 'min' is over all permutations Ys of Y.
%       If X and Y are matrices, d is the vector defined from columns:
%           d(1,j) = wasserstein2(X(:,j), Y(:,j)) for all j.
%       Note that the third argument is the same as for 'distance2'.
%
%   Note:
%       X and Y must have the same size.
%
%   See also:
%       distance2

    if (size(X) ~= size(Y))
        error('Dimension mismatch');
    elseif (~exist('normalized', 'var'))
        normalized = false;
    end;

    XN = X + 1e-12 * rand(size(X));
    [osef, I] = sort(XN, 1);
    J = repmat(1:size(X, 2), size(X, 1), 1);
    sub = sub2ind(size(X), I, J);

    Xeq = zeros(size(X));
    Xeq(sub) = sort(Y, 1);

    [d, D, Ds] = distance2(X, Xeq, normalized);

end
