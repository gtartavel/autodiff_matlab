function CF = CellFunction(toArray, arg, varargin)
% CELL FUNCTION     - extend a function to deal with cells.
%
%   Usage:
%       CF = CellFunction(toArray, F);
%       CF = CellFunction(toArray, ...);
%
%   Arguments:
%       toArray - If true, CF returns an array (F must be a scalar function).
%                 If false, CF returns a cell.
%                 Same as 'UniformOutput' of Matlab's 'cellfun' function.
%       F       - A Function (see Function class) or an array of Functions.
%       ...     - Any set of arguments for Function (see Function constructor).
%                 Arguments can be cells.
%       CF      - A Function, taking as argument a cell.
%
%   Examples:
%
%       % Cell extension of F, element-wise:
%       CF = CellFunction(false, F);
%
%       % L2 distance to the cell C, element-wise:
%       CellDist = CellFunction(true, @distance2, C);   % out=array
%
%       % L2-norm function:
%       Norm2 = Function(@distance2, 0);     	% in=array, out=array
%       CellNorm2 = CellFunction(true, Norm2);  % in=cell , out=array
%

    % Compute cells dimensions
    if (iscell(arg))
        D = size(arg);
    else
        D = [];
    end;
    for k = 1:length(varargin)
        if (iscell(varargin{k}))
            if (isempty(D))
                D = size(varargin{k});
            elseif (~isequal(D, size(varargin{k})))
                error('Cell dimension must agree.');
            end;
        end;
    end;
    N = prod(D);
    Nargs = length(varargin);

    % Create function(s)
    if (isempty(D))
        F = Function(arg, varargin{:});
    else
        F = cell(D);
        args = cell(Nargs + 1, 1);
        for k = 1:N
            args{1} = getCell(arg, k);
            for karg = 1:Nargs
                args{karg + 1} = getCell(varargin{karg}, k);
            end;
            F{k} = Function(args{:});
        end;
    end;
    CF = Function(@evalCellFunction, F, toArray);

end

% Evaluate function(s) in value(s)
function [Y, DY, DYs] = evalCellFunction(X, F, toArray)

    % Get dimensions
    D = cellSize(X, F);
    N = prod(D);

    % Output arrays
    Y = cell(D);
    fcnDY = cell(D);
    fcnDYs = cell(D);

    % Function for adjoint
    if (toArray)
        getAdj = @getArray;
    else
        getAdj = @getCell;
    end;

    % Evaluate the function on each element
    for k = 1:N
        [Y{k}, DYk, DYsk] = getCell(F, k).eval(getCell(X, k), 1, 0, 1, 1);
        fcnDY{k} = @(dX) DYk(getCell(dX, k));
        fcnDYs{k} = @(dY) DYsk(getAdj(dY, k));
    end;

    % Factorize the argument from the cell of handles
    DY = @(dX) cellfun(@(fcn) fcn(dX), fcnDY, 'UniformOutput', toArray);
    DYs = @(dY) cellfun(@(fcn) fcn(dY), fcnDYs, 'UniformOutput', false);

    % Convert to array if needed
    if (toArray)
        Y = cell2mat(Y);
    end;

end

% Size of cells, error if not compatible
function d = cellSize(A, B)
    if (~iscell(A))
        if (~iscell(B))
            d = [1 1];
        else
            d = size(B);
        end;
    else
        d = size(A);
        if (iscell(B) && ~isequal(size(B), d))
            error('Cell dimensions must agree.');
        end;
    end;
end

% Get the k-th element of a cell, or the element itself if not a cell
function elmt = getCell(C, k)
    if (iscell(C))
        elmt = C{k};
    else
        elmt = C;
    end;
end

% Get the k-th element of an array, or the element itself if not an array
function elmt = getArray(A, k)
    if (isscalar(A))
        elmt = A;
    else
        elmt = A(k);
    end;
end