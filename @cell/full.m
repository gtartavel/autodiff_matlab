function Y = full(X)
% FULL      - convert a cell's sub-arrays to full (non-sparse) arrays.
% 
%   Usage:
%       Y = full(X);
    Y = cellop({@full X});
end
