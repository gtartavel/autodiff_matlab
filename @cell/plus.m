function C = plus(A, B)
% PLUS (+)      - element-wise addition between cells, or between a cell and a scalar.
%
%   Usage:
%       C = A + B;  % A or B may be a scalar
%       C = plus(A, B);
   C = cellscalarop({@plus A B});
end
