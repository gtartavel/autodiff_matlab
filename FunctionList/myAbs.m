function [Y, DY, DYs] = myAbs(X)
% MY ABS    - absolute value (revisited)
%
%   Usage:
%       Y = myAbs(X);       % same as Y = abs(X);
%       [Y, DY, DYs] = myAbs(X)

    Y = abs(X);
    DY = @(dX) sign(X) .* dX;
    DYs = DY;

end