function C = rdivide(A, B)
% RDIVIDE (./)   - element-wise right division between cells, or between a cell and a scalar.
%
%   Usage:
%       C = A ./ B;  % A or B may be a scalar
%       C = rdivide(A, B);
   C = cellscalarop({@rdivide A B});
end
