function [Y, DY, DYs] = mySum(X, dim)
% MY SUM    - compute the sum of all the elements, or along a dimension.
%
%   Usage:
%       y = mySum(X, []);       % same as y = sum(X(:))
%       Y = mySum(X, dim);      % same as Y = sum(X, dim);

    if (isempty(dim))
        Y = sum(X(:));
        DY = @(dX) sum(dX(:));
        DYs = @(dy) dy * ones(size(X));
    else
        repdim = ones(1, ndims(X));
        if (dim <= length(repdim))
            repdim(dim) = size(X, dim);
        end;
        Y = sum(X, dim);
        DY = @(dX) sum(dX, dim);
        DYs = @(dY) repmat(dY, repdim);
    end;

end