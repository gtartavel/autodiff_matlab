function C = ge(A, B)
% GE (>=)   - element-wise '>=' between cells, or between a cell and a scalar.
%
%   Usage:
%       C = A >= B;  % A or B may be a scalar
%       C = (A >= B);
%       C = ge(A, B);
    C = cellscalarop({@ge A B});
end
