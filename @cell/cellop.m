function out = cellop(arg)
% This is an auxiliary function, you should not use it directly.

% CELL OP       - apply an operator to cells
%
%   Usage:
%       C = cellop({op, A, B, ...});
%
%   Example:
%       C = cellop({@plus, A, B});      % Element-wise C = A + B

    out = cellfun(arg{1}, arg{2:end}, 'UniformOutput', false);

end