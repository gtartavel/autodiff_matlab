function C = minus(A, B)
% MINUS (-)   - element-wise difference between cells, or between a cell and a scalar.
%
%   Usage:
%       C = A - B;  % A or B may be a scalar
%       C = minus(A, B);
   C = cellscalarop({@minus A B});
end
