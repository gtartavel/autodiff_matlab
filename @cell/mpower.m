function C = mpower(A, B)
% MPOWER (^)    - matrix power between cells' sub-arrays, or between a cell and a scalar.
%
%   Usage:
%       C = A ^ B;  % A or B may be a scalar
%       C = power(A, B);
    C = cellscalarop({@mpower A B});
end
