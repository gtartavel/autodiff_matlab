function out = cellscalarop(arg)
% This is an auxiliary function, you should not use it directly.

% CELL SCALAR OP   - apply an operator to mixed scalars and cells
%
%   Usage:
%       C = cellscalarop({op, A, B, ...});
%
%   Example:
%       C = cellscalarop({@plus, A, B});     % Element-wise A + B
%       C = cellscalarop({@times, 2, A});    % Compute 2  * A
%       C = cellscalarop({@rdivide, A, 2});  % Compute A ./ 2

    % Get operator and arguments
    opc = @(C) arg{1}(C{:});
    args = arg(2:end);

    % Indices of non-scalar elements of 'args'
    isNotScalar = @(x) iscell(x) || ~isscalar(x);
    areCells = cellfun(isNotScalar, args);
    subsCells = struct('type', '()', 'subs', {{areCells}});

    % Restrict 'op' to the cells by assigning the scalar values
    opScalar = @(varargin) opc(subsasgn(args, subsCells, varargin));
    out = cellfun(opScalar, args{areCells}, 'UniformOutput', false);
end