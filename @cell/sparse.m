function Y = sparse(X)
% SPARSE    - convert a cell's sub-arrays to sparse arrays.
% 
%   Usage:
%       Y = sparse(X);
    Y = cellop({@sparse X});
end
