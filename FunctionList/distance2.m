function [d, D, Ds] = distance2(X, Y, normalized)
% DISTANCE 2    - squared L2 distance, column-wise.
%
% 	Usage:
%   	d = distance2(X, Y);
%   	d = distance2(X, Y, true);
%       [d, D, Ds] = distance2(...);
%
%   Return:
%       If X and Y are vectors, d is scalar, d = 1/2 |X-Y|^2.
%       If X and Y are matrices, d is the vector defined from columns:
%           d(1,j) = distance2(X(:,j), Y(:,j)) for all j.
%       Here |X-Y|^2 denotes by default the sum of squared difference.
%       However, if the 3rd argument is true, it is the mean squared error.
%
%   Note:
%       X and Y must have the same size.


    err = X - Y;
    if (~exist('normalized', 'var') || ~normalized)
        c = 1;
    else
        c = 1 / size(err, 1);
    end;

    d = 1/2 * c * sum(err .^ 2,  1);
    D = @(dX) c * sum(err .* dX, 1);
    Ds = @(dY) c * bsxfun(@times, dY, err);

end