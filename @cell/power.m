function C = power(A, B)
% POWER (.^)    - element-wise power between cells, or between a cell and a scalar.
%
%   Usage:
%       C = A .^ B;  % A or B may be a scalar
%       C = power(A, B);
    C = cellscalarop({@power A B});
end
